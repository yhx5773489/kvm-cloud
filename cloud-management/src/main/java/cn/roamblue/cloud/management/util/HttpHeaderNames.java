package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class HttpHeaderNames {
    /**
     * 登陆HTTP TOKEN 头
     */
    public static final String TOKEN_HEADER = "X-CLOUD-TOKEN";
    /**
     * 登陆用户HTTP 上下文
     */
    public static final String LOGIN_USER_ID_ATTRIBUTE = "X-CLOUD-USER-ID";
    /**
     * 语言
     */
    public static final String LANGUAGE = "X-Cloud-Language";
}
