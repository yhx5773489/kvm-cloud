package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class VmType {
    /**
     * 路由，负责DHCP功能
     */
    public static final String ROUTE = "Route";
    /**
     * 负责VNC
     */
    public static final String CONSOLE = "Console";
    /**
     * 用户自建VM
     */
    public static final String GUEST = "Guest";
}
