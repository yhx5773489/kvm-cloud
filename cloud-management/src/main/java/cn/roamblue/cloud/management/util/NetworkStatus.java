package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class NetworkStatus {
    /**
     *
     */
    public static final String READY = "Ready";
    public static final String PAUSE = "Pause";
}
