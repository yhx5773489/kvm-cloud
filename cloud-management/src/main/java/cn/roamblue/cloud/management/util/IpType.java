package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class IpType {
    public static final String MANAGER = "Manager";
    public static final String GUEST = "Guest";
}
