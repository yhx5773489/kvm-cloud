package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class UserState {
    /**
     * 启用
     */
    public static final short ABLE = 0;
    /**
     * 禁用
     */
    public static final short DISABLE = 1;
}
