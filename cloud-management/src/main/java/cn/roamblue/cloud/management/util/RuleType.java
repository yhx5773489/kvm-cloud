package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class RuleType {
    /**
     * 超级管理员
     */
    public static final int SUPER_ADMIN = 0;
    /**
     * 普通管理员
     */
    public static final int ADMIN = 100;
    /**
     * 普通用户
     */
    public static final int USER = 200;
}
