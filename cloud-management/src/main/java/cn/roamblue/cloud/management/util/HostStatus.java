package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class HostStatus {
    public static final String READY = "Ready";
    public static final String MAINTENANCE = "Maintenance";
}
