package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class Version {
    public static final String CURRENT_VERSION = "1.0.0";
}
