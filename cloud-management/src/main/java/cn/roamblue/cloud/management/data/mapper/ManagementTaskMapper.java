package cn.roamblue.cloud.management.data.mapper;

import cn.roamblue.cloud.management.data.entity.ManagementTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author chenjun
 */
@Repository
public interface ManagementTaskMapper extends BaseMapper<ManagementTaskEntity> {


}
