package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class TemplateType {
    /**
     * 系统route vm
     */
    public static final String ROUTE = "Route";
    /**
     * 系统console vm
     */
    public static final String CONSOLE = "Console";
    /**
     * 自定义磁盘模版
     */
    public static final String DISK = "Disk";
    /**
     * iso文件
     */
    public static final String ISO = "ISO";
}
