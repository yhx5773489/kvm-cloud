package cn.roamblue.cloud.management.util;

/**
 * @author chenjun
 */
public class TemplateStatus {
    /**
     * 模版初始化中
     */
    public static final String INIT = "Init";
    /**
     * 模版已就绪
     */
    public static final String READY = "Ready";
    /**
     * 模版出现错误
     */
    public static final String ERROR = "Error";
}
