package cn.roamblue.cloud.management.data.mapper;

import cn.roamblue.cloud.management.data.entity.PermissionCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author chenjun
 */
@Repository
public interface PermissionCategoryMapper extends BaseMapper<PermissionCategoryEntity> {

}
