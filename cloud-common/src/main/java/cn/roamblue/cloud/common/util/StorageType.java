package cn.roamblue.cloud.common.util;

/**
 * @ClassName: StorageType
 * @Description: TODO
 * @Create by: chenjun
 * @Date: 2021/8/5 上午11:12
 */
public class StorageType {
    public static final String NFS="nfs";
    public static final String LOCAL="local";
}
